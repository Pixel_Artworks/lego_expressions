﻿using UnityEngine;

[System.Serializable]
public class Pose
{
    public string name;
    [Range(0f, 100f)]
    public float valueMin = 0f;

    [Range(0f, 100f)]
    public float valueMax = 0f;

}
