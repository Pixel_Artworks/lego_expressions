﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RenderQueueOverride : MonoBehaviour
{

    public Int32 queue = 3001; 
    // Start is called before the first frame update
    void Start()
    {
        transform.GetComponent<Renderer>().material.renderQueue = queue;
        //transform.GetComponent<Renderer>().material.
        transform.GetComponent<Renderer>().sortingOrder = 100;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
