﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using RenderHeads.Media.AVProVideo;

public class FacialPoseEventBehaviour : MonoBehaviour
{
    private bool isChild = true;
    private bool isPlaying = false;

    [SerializeField]
    MediaPlayer mediaPlayer;

    [SerializeField]
    VideoStateManager videoStateManager;

    [SerializeField]
    VideoPlaybackCanvasVisibility videoPlaybackCanvasVisibility;

    [SerializeField]
    CanvasGroupController buttons_canvasGroupController;



    // Start is called before the first frame update
    void Start()
    {
    }

    private void Update()
    {
        //Debug.Log("isPlaying: " + isPlaying);
    }

    public void setIsChild(bool b) {
        isChild = b;
        //Debug.Log("isChild: " + isChild);
    }

    public void LoadFacialClip(int i) {

        if (isPlaying) return;

        //Debug.Log("Play The Facial Clip!");

        //NOTE: confirm if there will be more 'child versions', and change to i=i+3 if so
        if (i == 4 && isChild) {
            i = 7;
        }

        buttons_canvasGroupController.SetVisibility(false);
        videoStateManager.LoadClip(i);
        videoPlaybackCanvasVisibility.FadeInClip(0.125f);
        mediaPlayer.Rewind(true);
        mediaPlayer.Play();

        isPlaying = true;
        StartCoroutine(EnableFacialClips());

    }

    private IEnumerator EnableFacialClips() {

        float secs = mediaPlayer.Info.GetDurationMs() / 1000f;
        //Debug.Log("secs: " + secs);
        yield return new WaitForSeconds(secs + 2);
        isPlaying = false;
    }

}
