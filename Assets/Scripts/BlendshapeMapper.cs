﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlendshapeMapper : MonoBehaviour
{
    private SkinnedMeshRenderer this_skinnedMeshRenderer;

    [SerializeField] private SkinnedMeshRenderer master_skinnedMeshRenderer;

    public float blendshapeRangeRestrictor = 0.9f;
    public string[] blendshapeNamesToAffect;

    [SerializeField] private bool useRounding = false;
    [System.Serializable] public enum RoundingType { UP, DOWN, CLOSEST }
    public RoundingType roundingType;
    public float roundingMultiple = 20;

    // Start is called before the first frame update
    void Start()
    {
        this_skinnedMeshRenderer = transform.GetComponent<SkinnedMeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateLocalBlendshapeWeights();
    }

    private void UpdateLocalBlendshapeWeights() {
        if (!this_skinnedMeshRenderer)
            return;

        for (int i = 0; i < this_skinnedMeshRenderer.sharedMesh.blendShapeCount; i++)
        {
            for (int j = 0; j < master_skinnedMeshRenderer.sharedMesh.blendShapeCount; j++)
            {
                var n = master_skinnedMeshRenderer.sharedMesh.GetBlendShapeName(j);
                n = n.Remove(0, 11);

                if (this_skinnedMeshRenderer.sharedMesh.GetBlendShapeName(i).Contains(n))
                {

                    var index = this_skinnedMeshRenderer.sharedMesh.GetBlendShapeIndex(this_skinnedMeshRenderer.sharedMesh.GetBlendShapeName(i));

                    this_skinnedMeshRenderer.SetBlendShapeWeight(index, master_skinnedMeshRenderer.GetBlendShapeWeight(j));

                    for (int k = 0; k < blendshapeNamesToAffect.Length; k++) {
                        if (this_skinnedMeshRenderer.sharedMesh.GetBlendShapeName(i).ToString() == blendshapeNamesToAffect[k]) {

                            if (useRounding)
                            {
                                int r = (int)roundingType;
                                FloatExtensions.ROUNDING fr = (FloatExtensions.ROUNDING)r;
                                //Debug.Log("rounding fr: " + fr.ToString());

                                float v = master_skinnedMeshRenderer.GetBlendShapeWeight(j);
                                //Debug.Log("v: " + v);
                                if (v < 25)
                                {
                                    v = 0;
                                }
                                else {
                                    v = v.ToNearestMultiple((int)roundingMultiple, fr);
                                }
                                
                                this_skinnedMeshRenderer.SetBlendShapeWeight(index, v);
                            }
                            else {
                                this_skinnedMeshRenderer.SetBlendShapeWeight(index, master_skinnedMeshRenderer.GetBlendShapeWeight(j) * blendshapeRangeRestrictor);                            }
                        }
                    }
                    
                }
            }
        }
    }
}

public static class FloatExtensions
{
    public enum ROUNDING { UP, DOWN, CLOSEST }

    public static float ToNearestMultiple(this float f, int multiple, ROUNDING roundTowards = ROUNDING.CLOSEST)
    {
        f /= multiple;

        return (roundTowards == ROUNDING.UP ? Mathf.Ceil(f) : (roundTowards == ROUNDING.DOWN ? Mathf.Floor(f) : Mathf.Round(f))) * multiple;
    }

    /// <summary>
    /// Using a multiple with a maximum of two decimal places, will round to this value based on the ROUNDING method chosen
    /// </summary>
    public static float ToNearestMultiple(this float f, float multiple, ROUNDING roundTowards = ROUNDING.CLOSEST)
    {
        f = float.Parse((f * 100).ToString("f0"));
        multiple = float.Parse((multiple * 100).ToString("f0"));

        f /= multiple;

        f = (roundTowards == ROUNDING.UP ? Mathf.Ceil(f) : (roundTowards == ROUNDING.DOWN ? Mathf.Floor(f) : Mathf.Round(f))) * multiple;

        return f / 100;
    }
}
