﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.pixelartworks.legoexpressions
{
    public class EX_FacePoseEvents : MonoBehaviour
    {

        [SerializeField] SOEvent so_facePoseEvent;
        private SkinnedMeshRenderer skinnedMeshRenderer;

        [SerializeField]public string blendShapeToTrack = "blendShape1.tongueOut";

        void Start()
        {
            skinnedMeshRenderer = transform.GetComponent<SkinnedMeshRenderer>();
        }

        void Update()
        {
            CompareBlendShapeValues();
        }

        private void CompareBlendShapeValues()
        {
            var index = skinnedMeshRenderer.sharedMesh.GetBlendShapeIndex(blendShapeToTrack);
            if (skinnedMeshRenderer.GetBlendShapeWeight(index) > 50)
            {
                so_facePoseEvent.Raise();

            }
        }
    }
}
