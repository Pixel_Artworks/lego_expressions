﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

namespace com.pixelartworks.legoexpressions
{
    public class EX_FacialTrackingManagement : MonoBehaviour
    {
        [Header("Mobile Tracking Device")]
        [SerializeField] public ExtensionsToggle deviceSwitch;
        [SerializeField] private SOEvent_Bool so_faceIsTracked;
        private bool m_isTracked = false;
        public bool enableFacialTracking = true;
        public int currentDeviceIndex = 0;
        public bool IsTracked
        {
            get { return m_isTracked; }
            set
            {
                if (m_isTracked != value)
                {
                    m_isTracked = value;
                    Debug.Log("EX_FacialTrackingManagement: m_isTracked: " + m_isTracked);
                    so_faceIsTracked.Raise(!m_isTracked);
                }
            }
        }

        [Header("Scene Camera Rig Control")]
        [SerializeField] private Transform cameraRig;
        [SerializeField] private float device1_Pos = 0.25f;
        [SerializeField] private float device2_Pos = 0.32f;
        
        [Header("Debug UI")]
        [SerializeField] public CanvasGroup canvasGroup_FacialTracking;
        [SerializeField] private Toggle toggle_enableFacialTracking;
        [SerializeField] private Image networkMeshAnimatorStatus;

        void Start()
        {
            SetupUI();
            IsTracked = NetworkMeshAnimator.isTracking;
        }

        void Update()
        {
            if (m_isTracked != NetworkMeshAnimator.isTracking)
            {
                IsTracked = NetworkMeshAnimator.isTracking;
            }

            SetMeshAnimatorStatusUI();
        }

        public void CheckIfTracking()
        {
            IsTracked = NetworkMeshAnimator.isTracking;
            so_faceIsTracked.Raise(!IsTracked);
        }

        public void SetTrackingDevice(bool b)
        {
            if (!b)
            {
                currentDeviceIndex = 0;
                
            }
            else {
                currentDeviceIndex = 1;
            }

            transform.GetComponent<EX_ExpressionsSystem>().OnDeviceSelector(b);

            Debug.Log("EX_FacialTrackingManagement: Current Device Index: " + currentDeviceIndex);
            StartCoroutine(SwitchCamPosition(currentDeviceIndex));
        }

        private IEnumerator SwitchCamPosition(int i)
        {
            Debug.Log("EX_FacialTrackingManagement: Switch Cam Position");
            
            switch (i)
            {
                case 0:
                    cameraRig.DOLocalMoveY(device1_Pos, 0.5f);
                    //deviceSwitch.IsOn = false;
                    break;
                case 1:
                    cameraRig.DOLocalMoveY(device2_Pos, 0.5f);
                    break;
            }
            
            yield return new WaitForSeconds(1f);
        }
 
        private void SetupUI() {
            toggle_enableFacialTracking.onValueChanged.AddListener((bool b)=> {
                enableFacialTracking = b;
                if (enableFacialTracking)
                {
                    NetworkMeshAnimator.Instance.StartAcceptingMessages();
                }
                else {
                    NetworkMeshAnimator.Instance.StopAcceptingMessages();
                }
                StartCoroutine(DelaySetFaceTrackingAnimation(b));
            });

            canvasGroup_FacialTracking.interactable = false;

            deviceSwitch.onValueChanged.AddListener((bool b) =>
            {
                if (!b)
                {
                    SetTrackingDevice(false);
                }
                else
                {
                    SetTrackingDevice(true);
                }
                
                CheckIfTracking();
            });

            deviceSwitch.IsOn = true;
        }

        private void SetMeshAnimatorStatusUI()
        {
            if (NetworkMeshAnimator.Instance.isAcceptingMessages)
            {
                networkMeshAnimatorStatus.color = Color.green;
            }
            else {
                networkMeshAnimatorStatus.color = Color.red;
            }
        }

        private IEnumerator DelaySetFaceTrackingAnimation(bool b) {
            yield return new WaitForSeconds(0.5f);
            so_faceIsTracked.Raise(!b);
            yield break;
        }
        
    }
}
