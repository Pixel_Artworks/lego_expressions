﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Filters;

namespace com.pixelartworks.legoexpressions
{
    public class EX_GetFaceTransform : MonoBehaviour
    {
        [SerializeField] public Transform face_transform;
        [SerializeField] public Transform ragdoll_transform;
        public Vector3 minRotations = new Vector3(0f, 0f, 0f);
        public Vector3 maxRotations = new Vector3(0f, 0f, 0f);
        public Vector4 rotationMultiplier = new Vector4(1f, 1f, 1f, 1f);
        public float new_Q = 0.000001f;
        public float new_R = 0.01f;
        public float tween = 0.125f;
        private Transform sceneRig;
        private Transform camOffsetTransform;
        private float offsetYRot = 0f;
        private EX_ExpressionsSystem _EX_ExpressionsSystem;
        private Quaternion faceRotation;

        void OnEnable()
        {
            _EX_ExpressionsSystem = FindObjectOfType<EX_ExpressionsSystem>();
            sceneRig = GameObject.FindGameObjectWithTag("SceneRig").GetComponent<Transform>();
            camOffsetTransform = GameObject.FindGameObjectWithTag("CamOffsetTransform").GetComponent<Transform>();
            //UpdateOffset();
        }


        private void Update()
        {


            if (sceneRig != null && face_transform != null)
            {

                faceRotation = new Quaternion(face_transform.rotation.x * rotationMultiplier.x, face_transform.rotation.y * rotationMultiplier.y, face_transform.rotation.z * rotationMultiplier.z, face_transform.rotation.w * rotationMultiplier.w);

               
                Vector3 rot = faceRotation.eulerAngles;
                float y = rot.y;
                DOTween.To(() => y, x => y = x, y, tween);
                //Vector3 modified = new Vector3(rot.x, y, rot.z);
                Vector3 modified = new Vector3(rot.x, 0f, rot.z);

                //ragdoll_transform.DOLocalRotateQuaternion(faceRotation, tween);
                ragdoll_transform.DOLocalRotateQuaternion(Quaternion.Euler(modified), tween);

                offsetYRot = -ragdoll_transform.localRotation.eulerAngles.y;

            }
        }

        /*
        public void UpdateOffset()
        {
            Debug.Log("Start DOUpdateOffset");

            
            _EX_ExpressionsSystem.transition_canvas.enabled = true;
            _EX_ExpressionsSystem.transition_mediaPlayer.Play();
            _EX_ExpressionsSystemtransition_audioSource.Play();

        //TODO: SUGGEST EFFECT TO HIDE JUMP!
            

            StartCoroutine(DOUpdateOffset());
        }

        private IEnumerator DOUpdateOffset()
        {
            //yield return new WaitForSeconds(0.5f);
            camOffsetTransform.DOLocalRotateQuaternion(Quaternion.Euler(new Vector3(0f, offsetYRot, 0f)), tween);
            yield return new WaitForSeconds(0.75f);
            //yield break;
        }
        */
    
    }
}

    public static class ExtensionMethods
    {
        public static float Remap(this float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }
    }

