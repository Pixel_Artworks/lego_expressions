﻿using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsManager : Singleton<AnalyticsManager>
{
    public string ExperienceID = string.Empty;
    public string StoreID = string.Empty;
    public bool writeToLocalDataFile = true;

    private void Start()
    {
        ReportCustomEventTimestamp_Simple("ApplicationOpened");
    }


    public void ReportCustomEventTimestamp_Simple(string eventName)
    {
        string id = ExperienceID + "_" + StoreID;

        var data = new Dictionary<string, object>
        {
            { (id + "_" + eventName), DateTime.Now }
        };

        AnalyticsResult analytics_result = AnalyticsEvent.Custom(id, data);

        //Debug.Log("ReportCustomEventTimestamp: " + id + "_" + data +  " -- analytics_result: " + analytics_result );

        //Write to CSV
        if (!writeToLocalDataFile) return;

        var dataFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "/analytics_data";

        try
        {
            if (!Directory.Exists(dataFolder))
            {
                Directory.CreateDirectory(dataFolder);
                Debug.Log("Created analystics data location");
            }
            else
            {
                //Debug.Log("Analystics data location exists");
            }

            string csv = String.Join(
            Environment.NewLine,
            data.Select(d => $"{d.Key};{d.Value};")
            ); ;

            var dataFile = dataFolder + "/analytics.csv";

            if (!File.Exists(dataFile))
            {
                Debug.Log("Creating new analytics file");
                System.IO.File.WriteAllText(dataFile, csv);
            }
            else {
                //Debug.Log("Analytics file exists - appending new data");
                System.IO.File.AppendAllText(dataFile, csv + System.Environment.NewLine);
            }
            
        }
        catch (IOException e) {
            Debug.LogWarning(e.Message);
        }
        
        
        
        

    }

}
