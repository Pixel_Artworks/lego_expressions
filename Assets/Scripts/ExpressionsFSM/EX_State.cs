﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace com.pixelartworks.legoexpressions
{
    public abstract class EX_State : MonoBehaviour
    {
        protected EX_ExpressionsSystem EX_ExpressionsSystem;
        public EX_State(EX_ExpressionsSystem ex_ExpressionsSystem)
        {
            EX_ExpressionsSystem = ex_ExpressionsSystem;
        }

        public virtual IEnumerator Start()
        {
            yield break;
        }

        public virtual IEnumerator OnUserDetected()
        {
            yield break;
        }

        public virtual IEnumerator OnUserLost()
        {
            yield break;
        }

        public virtual IEnumerator StateTimeout()
        {
            yield break;
        }

        public virtual IEnumerator OnSwitchCharacter(bool b)
        {
            yield break;
        }

        public virtual IEnumerator OnSwitchDevice(bool b)
        {
            yield break;
        }


        public virtual IEnumerator OnFacialPoseEvent()
        {
            yield break;
        }


        public virtual IEnumerator Exit()
        {
            yield break;
        }
    }
}

