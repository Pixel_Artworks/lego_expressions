﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.pixelartworks.legoexpressions
{
    public class EX_Idle : EX_State
    {

        public EX_Idle(EX_ExpressionsSystem ex_ExpressionsSystem) : base(ex_ExpressionsSystem)
        {
        }

        public override IEnumerator Start()
        {
            Debug.Log(EX_ExpressionsSystem.status + "EX_Idle");

            EX_ExpressionsSystem.StartCharacterUIVisibility(false);
            EX_ExpressionsSystem.characterFacialEvent_canvas.enabled = false;
            EX_ExpressionsSystem.characterFacialEvent_mediaPlayer.Stop();
            //EX_ExpressionsSystem.characterFacialEvent_audioSource.Stop();

            foreach (Transform child in EX_ExpressionsSystem.CharacterParent) {
                GameObject.Destroy(child.gameObject);
            }

            EX_ExpressionsSystem.mainMedia_canvas.enabled = false;
            yield return new WaitForSeconds(1f);
            EX_ExpressionsSystem.mainMedia_canvas.enabled = true;
            EX_ExpressionsSystem.main_mediaPlayer.Rewind(true);
            EX_ExpressionsSystem.main_mediaPlayer.Play();

            EX_ExpressionsSystem.mainMedia_audioSource.loop = true;
            EX_ExpressionsSystem.mainMedia_audioSource.Play();

            yield break;
        }

        public override IEnumerator OnUserDetected()
        {
            Debug.Log("EX_Mocap" + " : OnUserDetected");

            EX_ExpressionsSystem.ExitState();
            yield break;
        }

        public override IEnumerator Exit()
        {
            Debug.Log("Exiting state: " + "EX_Idle");

            yield return new WaitForSeconds(0.5f);
            EX_ExpressionsSystem.mainMedia_canvas.enabled = false;
            EX_ExpressionsSystem.main_mediaPlayer.Rewind(true);
            EX_ExpressionsSystem.mainMedia_audioSource.Stop();

            EX_ExpressionsSystem.SetState(new EX_Mocap(EX_ExpressionsSystem));
            yield break;
        }
    }
}
