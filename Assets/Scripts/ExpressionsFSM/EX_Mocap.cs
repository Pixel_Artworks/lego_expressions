﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace com.pixelartworks.legoexpressions
{
    public class EX_Mocap : EX_State
    {

        private int characterIndex = 0;
        private int r_index = 0;
        private int last_r_index = 0;

        private GameObject character = null;

        public EX_Mocap(EX_ExpressionsSystem ex_ExpressionsSystem) : base(ex_ExpressionsSystem)
        {
        }

        public override IEnumerator Start()
        {
            Debug.Log(EX_ExpressionsSystem.status + "EX_Mocap");

            EX_ExpressionsSystem.StartCharacterUIVisibility(true);
            RandomizeCharacterIndex();

            //TODO: move this out of here
            FindObjectOfType<EX_FacialTrackingManagement>().canvasGroup_FacialTracking.interactable = true;
            
            EX_ExpressionsSystem.StateTimeout();
            yield break;
        }

        public override IEnumerator OnUserLost()
        {
            Debug.Log("EX_Mocap" + " : OnUserLost");

            EX_ExpressionsSystem.ExitState();
            yield break;
        }

        public override IEnumerator OnFacialPoseEvent()
        {
            Debug.Log("EX_Mocap" + " : OnFacialPoseEvent");

            EX_ExpressionsSystem.StartCharacterUIVisibility(false);
            EX_ExpressionsSystem.characterFacialEvent_canvas.enabled = true;
            EX_ExpressionsSystem.characterFacialEvent_mediaPlayer.Play();
            //EX_ExpressionsSystem.characterFacialEvent_audioSource.Play();
            yield break;
        }

        private void RandomizeCharacterIndex() {

            
            Debug.Log("Randomize Character");

            while (r_index == last_r_index)
            {
                r_index = Random.Range(0, EX_ExpressionsSystem.CharacterPrefabs.Count);
            }
            characterIndex = r_index;

            GameObject[] toggles = GameObject.FindGameObjectsWithTag("CharacterToggle");
            if (!toggles[r_index].GetComponent<ExtensionsToggle>().IsOn)
            {
                toggles[r_index].GetComponent<ExtensionsToggle>().IsOn = true;
                last_r_index = r_index;
            }
            else {
                toggles[0].GetComponent<ExtensionsToggle>().IsOn = true;
                last_r_index = 0;
            }
        }

        

        public override IEnumerator OnSwitchCharacter(bool b)
        {
            yield return new WaitForSeconds(1f);

            foreach (ExtensionsToggle t in GameObject.Find("Panel_Padding").GetComponentsInChildren<ExtensionsToggle>())
            {
                if (t.IsOn)
                {
                    string s = t.gameObject.name.Substring(t.gameObject.name.Length - 1);

                    try
                    {
                        characterIndex = int.Parse(s);
                    }
                    catch (System.FormatException e)
                    {
                        Debug.Log(e);
                    }
                }
            }

            SetCharacter(characterIndex);
            AnalyticsManager.Instance.ReportCustomEventTimestamp_Simple("SetCharacterEvent_CharacterIndex: " + characterIndex);
        }

        private void SetCharacter(int i) {
            RemoveCharacter();

            character = GameObject.Instantiate(EX_ExpressionsSystem.CharacterPrefabs[characterIndex], EX_ExpressionsSystem.CharacterParent);
            character.transform.SetParent(EX_ExpressionsSystem.CharacterParent);

            //Debug.Log("EX_ExpressionsSystem.CharacterParent children: " + EX_ExpressionsSystem.CharacterParent.childCount);

            if (FindObjectOfType<EX_FacialTrackingManagement>().enableFacialTracking) {
                NetworkMeshAnimator.Instance.StartAcceptingMessages();
            }
            
            //TODO: Move this out from here
            FindObjectOfType<EX_FacialTrackingManagement>().CheckIfTracking();

            //Set background color
            for (int j = 0; j < EX_ExpressionsSystem.characterBackgrounds.Length; j++)
            {
                if (j == i)
                {
                    EX_ExpressionsSystem.characterBackgrounds[j].SetActive(true);
                }
                else
                {
                    EX_ExpressionsSystem.characterBackgrounds[j].SetActive(false);
                }
            }

            LoadClip(i);
        }

        public override IEnumerator OnSwitchDevice(bool b)
        {
            LoadClip(characterIndex);
            yield break;
        }

        private void LoadClip(int i) {
            string clipPath = "Clips/" + EX_ExpressionsSystem.characterFacialEventClips[i] + ".mov";

            //TODO: Make this dynamic - currently Hardcoded for baseball man
            if (FindObjectOfType<EX_FacialTrackingManagement>().currentDeviceIndex == 1 && i == 1) {
                Debug.Log("Baseball Child height video clip set");
                if (EX_ExpressionsSystem.isChina)
                {
                    clipPath = "Clips/" + EX_ExpressionsSystem.characterFacialEventClips[3] + ".mov";
                }
                else {
                    clipPath = "Clips/" + EX_ExpressionsSystem.characterFacialEventClips[4] + ".mov";
                }
                
            }

            //Debug.Log("clipPath: " + clipPath);
            EX_ExpressionsSystem.characterFacialEvent_mediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, clipPath, false);
            //EX_ExpressionsSystem.characterFacialEvent_audioSource.clip = EX_ExpressionsSystem.characterFacialEventAudioClips[i];
        }

        private void RemoveCharacter() {
            /*
            if (character != null) {
                NetworkMeshAnimator.Instance.StopAcceptingMessages();
                GameObject.Destroy(character);
            }
            */

            NetworkMeshAnimator.Instance.StopAcceptingMessages();
            foreach (Transform child in EX_ExpressionsSystem.CharacterParent)
            {
                GameObject.Destroy(child.gameObject);
            }

            if (character != null) {
                character = null;
            }
        }

        public override IEnumerator Exit()
        {
            Debug.Log("Exiting state: " + "EX_Mocap");

            RemoveCharacter();
            EX_ExpressionsSystem.StartCharacterUIVisibility(false);

            //TODO: move this out from here
            FindObjectOfType<EX_FacialTrackingManagement>().canvasGroup_FacialTracking.interactable = false;
            
            EX_ExpressionsSystem.SetState(new EX_Idle(EX_ExpressionsSystem));
            yield break;
        }
    }
}

