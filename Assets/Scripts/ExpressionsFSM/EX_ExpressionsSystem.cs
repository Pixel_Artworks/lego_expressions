﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using RenderHeads.Media.AVProVideo;
using DG.Tweening;

namespace com.pixelartworks.legoexpressions
{
    public class EX_ExpressionsSystem : EX_StateMachine
    {
        [HideInInspector] public string status = "you are now in state: ";
        [HideInInspector] public Coroutine stateTimeoutCoroutine = null;

        [Header("State Management")]
        [SerializeField] public int mocapTimeout = 60;
        [SerializeField] public MediaPlayer main_mediaPlayer;
        [SerializeField] public Canvas mainMedia_canvas;
        [SerializeField] public AudioSource mainMedia_audioSource;
        [SerializeField] public MediaPlayer transition_mediaPlayer;
        [SerializeField] public Canvas transition_canvas;
        //[SerializeField] public AudioSource transition_audioSource;
        [SerializeField] public MediaPlayer characterFacialEvent_mediaPlayer;
        [SerializeField] public Canvas characterFacialEvent_canvas;
        [SerializeField] public CanvasGroup characterSelectionUI_canvas;
        //[SerializeField] public AudioSource characterFacialEvent_audioSource;

        [Header("Character Management")]
        [SerializeField] public Transform CharacterParent;
        [SerializeField] public List<GameObject> CharacterPrefabs = new List<GameObject>();
        [SerializeField] public GameObject[] characterBackgrounds;
        [SerializeField] public string[] characterFacialEventClips;
        //[SerializeField] public AudioClip[] characterFacialEventAudioClips;
        [SerializeField] public ExtensionsToggleGroup extensionsToggleGroup;
        public bool isChina = false;

        [Header("Debug UI")]
        [SerializeField] private Canvas configUI;
        private bool i_KeyToggle = false;
        [SerializeField] private Button button_idle;
        [SerializeField] private Button button_mocap;
        [SerializeField] private Slider slider_timeoutSeconds;
        [SerializeField] private TMP_Text text_timeoutSeconds;

        #region Execution
        void Start()
        {
            SetupUI();
            transition_mediaPlayer.Play();
            //transition_audioSource.Play();
            characterFacialEvent_canvas.enabled = false;

            characterSelectionUI_canvas.alpha = 0f;
            characterSelectionUI_canvas.interactable = false;
            characterSelectionUI_canvas.blocksRaycasts = false;

            SetState(new EX_Idle(this));
        }

        void Update()
        {
            KeyboardInput();
        }
        #endregion

        #region userIO

        public void OnUserDetected(){
            StartCoroutine(State.OnUserDetected());
            AnalyticsManager.Instance.ReportCustomEventTimestamp_Simple("UserDetected");
        }
        
        public void OnUserLost(){
            StartCoroutine(State.OnUserLost());
            AnalyticsManager.Instance.ReportCustomEventTimestamp_Simple("UserLost");
        }

        public void StateTimeout() {
            //stateTimeoutCoroutine = StartCoroutine(State.StateTimeout());
            if (!transform.GetComponent<EX_PlayerDetection>().enableTimeout) return;
            Debug.Log("EX_ExpressionsSystem: Starting timeout coroutine");
            stateTimeoutCoroutine = StartCoroutine(Timeout());
        }

        public void OnSwitchCharacter(bool b) {
            transition_canvas.enabled = true;
            transition_mediaPlayer.Play();
            //transition_audioSource.Play();

            StartCoroutine(State.OnSwitchCharacter(b));
        }

        public void OnFacialPoseEvent() {
            StartCoroutine(State.OnFacialPoseEvent());
            AnalyticsManager.Instance.ReportCustomEventTimestamp_Simple("FacialPoseEvent");
        }

        private IEnumerator Timeout() {
            yield return new WaitForSeconds(mocapTimeout);
            AnalyticsManager.Instance.ReportCustomEventTimestamp_Simple("ExperienceTimeout");
            ExitState();
        }

        public void ExitState() {
            if (stateTimeoutCoroutine != null)
            {
                Debug.Log("EX_ExpressionsSystem: Stopping timeout coroutine");
                StopCoroutine(stateTimeoutCoroutine);
            }

            transition_canvas.enabled = true;
            transition_mediaPlayer.Play();
            //transition_audioSource.Play();
            StartCoroutine(State.Exit());
        }
        #endregion

        #region configUI
        private void KeyboardInput() {

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }

            if (Input.GetKeyDown("i"))
            {
                i_KeyToggle = !i_KeyToggle;
                configUI.enabled = i_KeyToggle;
            }
        }

        public void SetupUI() {
            configUI.enabled = i_KeyToggle;

            button_idle.onClick.AddListener(()=>{
                ExitState();
            });

            button_mocap.onClick.AddListener(() =>{
                OnUserDetected();
            });

            
            extensionsToggleGroup.onToggleGroupChanged.AddListener((bool b) => {
                OnSwitchCharacter(b);
            });

            slider_timeoutSeconds.value = mocapTimeout;
            text_timeoutSeconds.text = mocapTimeout.ToString();
            slider_timeoutSeconds.onValueChanged.AddListener((float f)=> {
                mocapTimeout = ((int)f);
                text_timeoutSeconds.text = mocapTimeout.ToString();
            });
        }

        public void StartCharacterUIVisibility(bool b) {
            StartCoroutine(CharacterUIVisibility(b));
        }
        private IEnumerator CharacterUIVisibility(bool b)
        {
            if (b)
            {
                characterSelectionUI_canvas.DOFade(1f, 0.2f);
                characterSelectionUI_canvas.interactable = true;
                characterSelectionUI_canvas.blocksRaycasts = true;
            }
            else {
                characterSelectionUI_canvas.DOFade(0f, 0.2f);
                characterSelectionUI_canvas.interactable = false;
                characterSelectionUI_canvas.blocksRaycasts = false;
            }
            yield return new WaitForSeconds(0.2f);
        }

        public void OnDeviceSelector(bool b) {
            StartCoroutine(State.OnSwitchDevice(b));
        }


        #endregion
    }
}
