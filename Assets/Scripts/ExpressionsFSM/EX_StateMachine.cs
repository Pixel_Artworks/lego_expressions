﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.pixelartworks.legoexpressions
{
    public abstract class EX_StateMachine : MonoBehaviour
    {
        protected EX_State State;

        public void SetState(EX_State state)
        {
            State = state;
            StartCoroutine(State.Start());
        }
    }
}
