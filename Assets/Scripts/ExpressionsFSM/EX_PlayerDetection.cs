﻿using UnityEngine;
using System.Collections;
using com.rfilkov.kinect;
using System.Collections.Generic;
using DG.Tweening;
using com.rfilkov.components;
using UnityEngine.UI;
using TMPro;

namespace com.pixelartworks.legoexpressions
{
    public class EX_PlayerDetection : MonoBehaviour
    {
        public bool enableAzure = true;
        public bool enableTimeout = true;

        //User Detection
        [Header("Azure User Detection")]
        [Tooltip("Index of the player, tracked by this component. 0 means the 1st player, 1 - the 2nd one, 2 - the 3rd one, etc.")]
        public int playerIndex = 0;
        [Tooltip("Depth sensor index used for color frame overlay - 0 is the 1st one, 1 - the 2nd one, etc.")]
        public int sensorIndex = 0;
        [Tooltip("Delay in seconds when the replay starts, after no more users have been detected.")]
        public float userLostMaxTime = 1f;

        [SerializeField] private GameObject kinectController;
        [SerializeField] private Transform playerPositionParent;
        [SerializeField] private GameObject playerPositionCollider;
        [SerializeField] private Transform detectionZonePlane;
        [SerializeField] private GameObject detectionZone;
        [SerializeField] private SOEvent so_userDetected;
        [SerializeField] private SOEvent so_userLost;
        [SerializeField] private Transform sceneRig;
        private float rotY = 0f;
        private float smoothedValue = 0f;

        private GameObject m_playerPositionCollider;
        private KinectManager kinectManager = null;
        private Vector3 playerPosition;
        private float userDetectionConfidence = 0.0f;
        private float lastUserTime = 0f;
        private bool userDetected = false;
        private bool userIsInRange = false;
        private bool isUserDetected = false;
        private bool isUserHeightAboveThreshold = false;
        private float timeLeft = 5f;

        //User Height Detection
        [Header("User Height Analysis")]
        public float maxHeightThreshhold = 1.2f;
        [SerializeField] private ExtensionsToggle toggle_deviceSelector;

        private BodySlicer bodySlicer;
        private HeightEstimator heightEstimator;
        private float estimatedHeight;

        [Header("Debug UI")]
        [SerializeField] private Toggle toggle_enableAzure;
        [SerializeField] private Toggle toggle_enableTimeout;
        [SerializeField] private Slider slider_userLostMaxTime;
        [SerializeField] private TMP_Text text_userLostMaxTime;
        [SerializeField] private Image azureStatus;
        [SerializeField] private Canvas azureView;
        [SerializeField] private GameObject azureSkeletonOverlay;
        private bool a_KeyToggle = false;
        [SerializeField] Canvas[] multiCanvasControl;
        [SerializeField] private TMP_Text hitzonePosX;
        [SerializeField] private TMP_Text hitzonePosZ;
        [SerializeField] private Button button_reset;
        [SerializeField] private Transform debugOrientation;

        public bool IsUserDetected
        {
            get { return isUserDetected; }
            set
            {
                if (value == isUserDetected)
                    return;

                isUserDetected = value;
                if (isUserDetected)
                {
                    Debug.Log("User Found");
                    StartCoroutine(SampleHeight());
                    so_userDetected.Raise();
                }
                else
                {
                    Debug.Log("User Lost");
                    so_userLost.Raise();
                }
            }
        }

        public bool flipUserPosition_X = false;

        #region StandardMethods
        private void Awake()
        {
            if (!enableAzure) {
                kinectController.SetActive(false);
            }

            StartCoroutine(EnableAzure(enableAzure));
        }

        void Start()
        {
            //Debug UI
            SetupUI();

            //User Detection

            kinectManager = KinectManager.Instance;

            playerPosition = new Vector3(0f, 0f, 0f);

            if (kinectManager && kinectManager.IsInitialized())
            {
                m_playerPositionCollider = Instantiate(playerPositionCollider) as GameObject;
                //m_playerPositionCollider.transform.parent = transform;
                m_playerPositionCollider.transform.parent = playerPositionParent;
                m_playerPositionCollider.SetActive(false);

                if (!userIsInRange)
                {
                    detectionZone.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
                }
                else {
                    detectionZone.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
                }
            }

            if (kinectManager && !kinectManager.IsInitialized())
            {
                Debug.Log("azure not initialized - probably no azure attached");
                enableAzure = false;
                toggle_enableAzure.isOn = enableAzure;
            }

            timeLeft = userLostMaxTime;

            //User Height Detection

            bodySlicer = transform.GetComponent<BodySlicer>();
            bodySlicer.enabled = false;
            bodySlicer.enabled = true;

            heightEstimator = transform.GetComponent<HeightEstimator>();
        }

        void Update()
        {
            if (kinectManager && kinectManager.IsInitialized())
            {
                ulong userId = kinectManager.GetUserIdByIndex(playerIndex);
                //Check if kinect can see a user
                if (kinectManager.IsUserDetected(playerIndex))
                {
                    //ulong userId = kinectManager.GetUserIdByIndex(playerIndex);
                    playerPosition = kinectManager.GetUserPosition(userId);
                    float x = playerPosition.x;
                    if (flipUserPosition_X) {
                        x = -x;
                    }

                    Vector3 modifiedPosition = new Vector3(x, 0f, playerPosition.z);
                    m_playerPositionCollider.transform.position = modifiedPosition;

                    //Create a confidence score for the detection value as the data can be very noisy
                    StartCoroutine(DetectionConfidenceSmoothing(1f));
                    userDetected = true;

                    //Scene Camera Rig Rotation
                    Quaternion rot = kinectManager.GetUserOrientation(userId, false);
                    //Quaternion rot = kinectManager.GetJointOrientation(userId, 26, false); //Head Joint: docs.microsoft.com/en-us/azure/kinect-dk/body-joints
                    Vector3 vRot = rot.eulerAngles;
                    rotY = vRot.y;

                }
                else {
                    StartCoroutine(DetectionConfidenceSmoothing(0f));
                }

                //If the confidence score is above a certain threshold keep our player alive
                if (userDetectionConfidence > 0.01f) {
                    m_playerPositionCollider.SetActive(true);
                }
                else {
                    m_playerPositionCollider.SetActive(false);

                    userDetected = false;
                    userIsInRange = false;
                }

                //Checking if play position within range / distance from hit zone
                if (userDetected) {
                    float dist = Vector3.Distance(m_playerPositionCollider.transform.position, detectionZone.transform.position);

                    if (dist < (detectionZone.transform.localScale.x + detectionZone.transform.localScale.z) / 2)
                    {
                        userIsInRange = true; }
                    else {
                        userIsInRange = false;
                    }
                }

                //Scene Camera Rig Rotation
                /*
                Vector3 modifiedOrientation = new Vector3(0f, rotY, 0f);
                Vector3 modifiedInverseOrientation = new Vector3(0f, -rotY, 0f);

                Quaternion clamped = ClampRotation(Quaternion.Euler(modifiedInverseOrientation), new Vector3(360f, 45f, 360f));
                Quaternion clampedInverse = ClampRotation(Quaternion.Euler(modifiedInverseOrientation), new Vector3(360f, 45f, 360f));

                debugOrientation.DOLocalRotateQuaternion(clamped, 0.25f);
                sceneRig.DOLocalRotateQuaternion(clampedInverse, 0.25f);
                */

                //Status and IO
                SetAzureStatusUI();
                KeyboardInput();


                //Timeout system for if a user suddenly drops out of range
                if (userIsInRange)
                {
                    detectionZone.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
                    //if (a_KeyToggle) return;

                    timeLeft = userLostMaxTime;
                    IsUserDetected = userIsInRange;
                    kinectManager.ClearKinectUsers();
                }
                else {

                    if (!enableTimeout) return;

                    timeLeft -= Time.deltaTime;
                    if (timeLeft < 0)
                    {
                        detectionZone.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
                        if (a_KeyToggle) return;

                        IsUserDetected = userIsInRange;

                    }
                }
            }
        }

        private void OnDestroy()
        {
            Destroy(m_playerPositionCollider);
        }
        #endregion

        private IEnumerator DetectionConfidenceSmoothing(float n) {
            DOTween.To(() => userDetectionConfidence, x => userDetectionConfidence = x, n, 0.25f);
            yield return new WaitForSeconds(0.25f);
        }

        IEnumerator SampleHeight()
        {
            Debug.Log("Sampling height...");
            float duration = Time.time + 1.5f;
            while (Time.time < duration)
            {
                estimatedHeight = heightEstimator.userHeight;
                yield return null;
            }

            estimatedHeight = heightEstimator.userHeight;

            if (estimatedHeight > maxHeightThreshhold)
            {
                Debug.Log("User is above height threshold");
                isUserHeightAboveThreshold = true;
                toggle_deviceSelector.IsOn = false;
            }
            else
            {
                Debug.Log("User is below height threshold");
                isUserHeightAboveThreshold = false;
                toggle_deviceSelector.IsOn = true;
            }
            yield break;
        }

        /*
        public void ResetCameraRotation() {
            StartCoroutine(DOResetCameraRotation());
        }

        private IEnumerator DOResetCameraRotation() {
            Vector3 modifiedOrientation = new Vector3(0f, 0f, 0f);
            Vector3 modifiedInverseOrientation = new Vector3(0f, 0f, 0f);

            Quaternion clamped = ClampRotation(Quaternion.Euler(modifiedInverseOrientation), new Vector3(360f, 45f, 360f));
            Quaternion clampedInverse = ClampRotation(Quaternion.Euler(modifiedInverseOrientation), new Vector3(360f, 45f, 360f));

            debugOrientation.DOLocalRotateQuaternion(clamped, 0.2f);
            sceneRig.DOLocalRotateQuaternion(clampedInverse, 0.2f);
            yield return new WaitForSeconds(0.2f);
        }
        */

        private void KeyboardInput()
        {

            if (Input.GetKeyDown("a"))
            {
                a_KeyToggle = !a_KeyToggle;

                azureView.enabled = a_KeyToggle;
                azureSkeletonOverlay.SetActive(a_KeyToggle);

                foreach (Canvas c in multiCanvasControl)
                {
                    c.enabled = !a_KeyToggle;
                }
            }

            if (Input.GetKeyDown("1"))
            {
                detectionZone.transform.localPosition = new Vector3(detectionZone.transform.localPosition.x + 0.05f, detectionZone.transform.localPosition.y, detectionZone.transform.localPosition.z);
            }
            if (Input.GetKeyDown("2"))
            {
                detectionZone.transform.localPosition = new Vector3(detectionZone.transform.localPosition.x - 0.05f, detectionZone.transform.localPosition.y, detectionZone.transform.localPosition.z);
            }
            if (Input.GetKeyDown("3"))
            {
                detectionZone.transform.localPosition = new Vector3(detectionZone.transform.localPosition.x, detectionZone.transform.localPosition.y, detectionZone.transform.localPosition.z + 0.05f);
            }
            if (Input.GetKeyDown("4"))
            {
                detectionZone.transform.localPosition = new Vector3(detectionZone.transform.localPosition.x, detectionZone.transform.localPosition.y, detectionZone.transform.localPosition.z - 0.05f);
            }

            //Local Rotation in X
            if (Input.GetKeyDown("5"))
            {
                detectionZonePlane.Rotate(Vector3.right, 1);
            }

            if (Input.GetKeyDown("6"))
            {
                detectionZonePlane.Rotate(Vector3.right, -1);
            }

            //TODO: implement pivot control here

            hitzonePosX.text = detectionZone.transform.localPosition.x.ToString();
            hitzonePosZ.text = detectionZone.transform.localPosition.z.ToString();

            PlayerPrefs.SetFloat("p_hitZoneX", detectionZone.transform.localPosition.x);
            PlayerPrefs.SetFloat("p_hitZoneZ", detectionZone.transform.localPosition.z);
            PlayerPrefs.SetFloat("p_hitPlaneRotX", detectionZonePlane.transform.localRotation.x);
        }

        #region DebugUI
        private void SetupUI() {
           
            toggle_enableAzure.isOn = enableAzure;
            toggle_enableAzure.onValueChanged.AddListener((bool b)=> {

                /*
                if (kinectManager && !kinectManager.IsInitialized())
                {
                    Debug.Log("azure not initialized - probably no azure attached");
                    enableAzure = false;
                    toggle_enableAzure.isOn = enableAzure;
                }
                */

                StartCoroutine(EnableAzure(b));
            });

            toggle_enableTimeout.isOn = enableTimeout;
            toggle_enableTimeout.onValueChanged.AddListener((bool b) => {
                enableTimeout = b;
            });


            slider_userLostMaxTime.value = userLostMaxTime;
            text_userLostMaxTime.text = userLostMaxTime.ToString();
            slider_userLostMaxTime.onValueChanged.AddListener((float f) => {
                userLostMaxTime = ((int)f);
                text_userLostMaxTime.text = userLostMaxTime.ToString();
                PlayerPrefs.SetFloat("p_userLostMaxTime", userLostMaxTime);
            });

            if (PlayerPrefs.HasKey("p_userLostMaxTime")) {
                userLostMaxTime = PlayerPrefs.GetFloat("p_userLostMaxTime", 1f);
                text_userLostMaxTime.text = userLostMaxTime.ToString();
            }

            azureView.enabled = false;
            azureSkeletonOverlay.SetActive(false);

            hitzonePosX.text = detectionZone.transform.localPosition.x.ToString();
            hitzonePosZ.text = detectionZone.transform.localPosition.z.ToString();

            button_reset.onClick.AddListener(()=> {
                detectionZone.transform.localPosition = new Vector3(0f, detectionZone.transform.localPosition.y, 1.5f);
                detectionZonePlane.localRotation = Quaternion.Euler(0f, 0f, 0f);

                PlayerPrefs.SetFloat("p_hitZoneX", detectionZone.transform.localPosition.x);
                PlayerPrefs.SetFloat("p_hitZoneZ", detectionZone.transform.localPosition.z);
                PlayerPrefs.SetFloat("p_hitPlaneRotX", detectionZonePlane.transform.localRotation.x);

            });
            
            detectionZone.transform.localPosition = new Vector3(PlayerPrefs.GetFloat("p_hitZoneX", 0f), detectionZone.transform.localPosition.y, PlayerPrefs.GetFloat("p_hitZoneZ", 1.5f));
            detectionZonePlane.localRotation = Quaternion.Euler(PlayerPrefs.GetFloat("p_hitPlaneRotX", 0f), detectionZonePlane.transform.localRotation.y, detectionZonePlane.transform.localRotation.z);

        }

        private IEnumerator EnableAzure(bool b) {

            if (b)
            {
                if (!kinectController.activeSelf) {
                    kinectController.SetActive(true);
                    yield return new WaitForSeconds(0.5f);
                }

                if (kinectManager)
                {
                    kinectManager.StartDepthSensors();
                }
            }
            else {
                yield return new WaitForSeconds(1f);
                if (kinectManager)
                {
                    kinectManager.StopDepthSensors();
                }
            }

            SetAzureStatusUI();
            playerPositionParent.gameObject.SetActive(b);

            enableAzure = b;
            yield break;
        }

        private void SetAzureStatusUI()
        {
            if (kinectManager && kinectManager.IsInitialized())
            {
                azureStatus.color = Color.green;
            }
            else
            {
                azureStatus.color = Color.red;
            }
        }

        #endregion

        public static Quaternion ClampRotation(Quaternion q, Vector3 bounds)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);
            angleX = Mathf.Clamp(angleX, -bounds.x, bounds.x);
            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            float angleY = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.y);
            angleY = Mathf.Clamp(angleY, -bounds.y, bounds.y);
            q.y = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleY);

            float angleZ = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.z);
            angleZ = Mathf.Clamp(angleZ, -bounds.z, bounds.z);
            q.z = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleZ);

            return q.normalized;
        }
    }
}
