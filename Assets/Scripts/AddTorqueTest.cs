﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddTorqueTest : MonoBehaviour
{


    [SerializeField]
    private Transform followObject;

    public float speed;
    public float angularSpeed;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        speed = rb.velocity.magnitude;
        angularSpeed = rb.angularVelocity.magnitude;

        rb.AddTorque(Vector3.forward);
    }
}
