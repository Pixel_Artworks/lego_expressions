﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State  
{
    protected CharacterSystem CharacterSystem;
    protected bool UserDetected;

    public State(CharacterSystem characterSystem, bool userDetected) {
        CharacterSystem = characterSystem;
        UserDetected = userDetected;
    }

    public virtual IEnumerator Start() {
        yield break;
    }

    public virtual IEnumerator Idle()
    {
        yield break;
    }

    public virtual IEnumerator Transition()
    {
        yield break;
    }

    public virtual IEnumerator Mocap()
    {
        yield break;
    }

    public virtual IEnumerator Timeout()
    {
        yield break;
    }

    public virtual IEnumerator Conversion()
    {
        yield break;
    }

    public virtual IEnumerator ExitState()
    {
        yield break;
    }
}
