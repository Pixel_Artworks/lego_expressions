﻿using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class IdleState : State
{
    private GameObject character;

    public IdleState(CharacterSystem characterSystem, bool userDetected) : base(characterSystem, userDetected)
    {
    }
    // Start is called before the first frame update
    public override IEnumerator Start()
    {

        if (CharacterSystem.stateLogConsole) {
            UnityEngine.Debug.Log("Idle");
        }

        CharacterSystem.Idle();
        yield break;
    }

    public override IEnumerator Idle()
    {
        CharacterSystem.soSetVideoState.Raise(0);

        if (UserDetected) {
            yield return new WaitForSeconds(CharacterSystem.idleStateTimeout);
            CharacterSystem.SetTransitionState();
        }
        yield break;
    }

    public override IEnumerator ExitState() {
        //UnityEngine.Debug.Log("EXIT IdleState");
        yield break;
    }
}
