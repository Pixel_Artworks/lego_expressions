﻿using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class TransitionState : State
{

    public TransitionState(CharacterSystem characterSystem, bool userDetected) : base(characterSystem, userDetected)
    {
    }

    public override IEnumerator Start()
    {

        if (CharacterSystem.stateLogConsole)
        {
            UnityEngine.Debug.Log("Transition");
        }

        CharacterSystem.Transition();
        yield break;
    }

    public override IEnumerator Transition()
    {
        CharacterSystem.soSetVideoState.Raise(1);

        yield return new WaitForSeconds(CharacterSystem.transitionStateDuration);
        if (UserDetected)
        {
            CharacterSystem.ExitState();
            CharacterSystem.soSampleUserHeight.Raise();

            CharacterSystem.RandomizeCharacter(); //NOTOE: Calls Toggles -> SetMocapState()
            //CharacterSystem.SetMocapState();
        }
        else {
            //CharacterSystem.ExitState();
            CharacterSystem.SetIdleState();
        }
        yield break;
    }

    public override IEnumerator ExitState() {
        //UnityEngine.Debug.Log("EXIT TransitionState");
        yield break;
    }
}
