﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public static class SceneLoader
{
    private class LoadingMonoBehaviour : MonoBehaviour { }

    public enum Scene {
        IdleScene,
        MocapScene,
        ConversionScene,
        SceneLoading
    }

    //public static string webViewURL = "";
 
    private static Action onLoaderCallback;
    private static AsyncOperation loadingAsyncOperation;


    public static void LoadScene(Scene scene, String url) {

        //webViewURL = url;

        //Set the loader callback action to load the target scene
        onLoaderCallback = () =>
        {
            GameObject loadingGameObject = new GameObject("Loading Game Object");
            loadingGameObject.AddComponent<LoadingMonoBehaviour>().StartCoroutine(LoadSceneAsync(scene));
        };

        //Load the loading scene
        SceneManager.LoadScene(Scene.SceneLoading.ToString());

    }

    private static IEnumerator LoadSceneAsync(Scene scene) {
        yield return null;

        loadingAsyncOperation =  SceneManager.LoadSceneAsync(scene.ToString());

        while (!loadingAsyncOperation.isDone) {
            yield return null;
        }
    }

    public static float GetLoadingProgress() {
        if (loadingAsyncOperation != null)
        {
            return loadingAsyncOperation.progress;
        }
        else {
            return 1f;
        }
    }

    public static void SceneLoaderCallback() {
        //Trigger after the first update which lets the screen refresh
        //Execute the loader callback action which will load the target scene
        if (onLoaderCallback != null) {
            onLoaderCallback();
            onLoaderCallback = null;
        }
    }

    public static string GetCurrentScene() {
        string scene = SceneManager.GetActiveScene().name;
        return scene;
    }
}


