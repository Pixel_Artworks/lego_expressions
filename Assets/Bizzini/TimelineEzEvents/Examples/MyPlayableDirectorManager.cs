﻿using Bizzini.TimelineEzEvents;
using Bizzini.TimelineEzEvents.Debugging;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

public class MyPlayableDirectorManager : MonoBehaviour
{
    private PlayableDirector director;
    public enum MyEnum { One, Two, Three };
    public Text OutputText;
    public AnimationCurve SphereMoveCurve;
    public TimelineEzEventsContainer MyCustomEventsContainer;

    void Awake()
    {
        director = GetComponent<PlayableDirector>();
        MyCustomEventsContainer.Init();
        if (Debug.isDebugBuild)
            TimelineEzEventsDebugger.AddListener_OnMethodInvoke(onMethodCalled);   //you won't need these, it's just for debug
        else
            OutputText.text = "Logs are visible on Development builds only";
    }

    void Start()
    {
        MyCustomEventsContainer.InvokeEvents();
        director.Play();
    }

    public void MoveMyObject(FrameData frameData, EzEvent ezEvent, UnityEngine.Object myObj, Vector3 startPos, Vector3 endPos)
    {
        GameObject go = (myObj as GameObject);
        float normTime = (float)(ezEvent.LinkedEzClip.LinkedTimelineClip.ToLocalTime(ezEvent.LinkedPlayableDirector.time) / ezEvent.LinkedEzClip.LinkedTimelineClip.duration);
        float curvedTime = SphereMoveCurve.Evaluate(normTime);
        go.transform.position = startPos + ((endPos - startPos) * curvedTime);
    }

    Material tempMaterial;
    public void ColorMyObject(FrameData frameData, EzEvent ezEvent, UnityEngine.Object myObj, Color startColor, Color endColor)
    {
        GameObject go = (myObj as GameObject);
        float normTime = (float)(ezEvent.LinkedEzClip.LinkedTimelineClip.ToLocalTime(ezEvent.LinkedPlayableDirector.time) / ezEvent.LinkedEzClip.LinkedTimelineClip.duration);
        float curvedTime = SphereMoveCurve.Evaluate(normTime);
        if (tempMaterial == null)
            tempMaterial = new Material(go.GetComponent<Renderer>().sharedMaterial);
        go.GetComponent<Renderer>().material = tempMaterial;
        tempMaterial.color = Color.Lerp(startColor, endColor, curvedTime);
    }

    public enum ResultEnum { IsTrue, IsFalse };
    public bool CheckMyCondition(ResultEnum result)
    {
        if (result == ResultEnum.IsTrue)
            return true;
        else
            return false;
    }

    public void GoToTime(float seconds, bool relative)
    {
        TimelineEzEventsHelper.TimelineGoToTime(director, seconds, relative);
    }

    public void PauseTimeline(EzEvent ezEvent, float seconds, bool resumeThen, bool moveToClipStart)
    {
        director.Pause();
        if (moveToClipStart && ezEvent.LinkedEzClip != null)
            TimelineEzEventsHelper.TimelineGoToTime(director, (float)ezEvent.LinkedEzClip.LinkedTimelineClip.start, false);
        if (resumeThen)
            StartCoroutine(waitAndResume(seconds));
    }

    private IEnumerator waitAndResume(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        director.Resume();
    }

    public void StartEmission(UnityEngine.Object myObj)
    {
        GameObject go = (myObj as GameObject);
        ParticleSystem ps = go.GetComponent<ParticleSystem>();
        ps.Play();
    }

    public static void ExampleStaticMethod(UnityEngine.Object myObject, string myString)
    {
        Debug.Log(myString);
    }

    public void ExampleMethod(FrameData frameData, Color myColor, uint myUint, Int64 myLong, string myString, UnityEngine.Object myObject, UnityEngine.Object someOtherObject)
    {
    }

    private void onMethodCalled(object sender, TimelineEzEventsDebugger.MethodInvokeEventArg e)   //you won't need this, it's just for debug
    {
        if (OutputText != null)
            OutputText.text = e.outputText + "\n";
    }
}
