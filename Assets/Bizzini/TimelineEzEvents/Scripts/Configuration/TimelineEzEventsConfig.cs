using System;
using UnityEditor;
using UnityEngine;

namespace Bizzini.TimelineEzEvents.EditorStuff
{
#if UNITY_EDITOR
    public static class TimelineEzEventsConfig
    {
        private static TimelineEzEventsConfigurationAsset _config;
        public const string StoreFolder = "TimelineEzEvents";
        private const string configFileName = "Configuration.asset";

        public static void Save()
        {
            if (_config == null)
            {
                _config = ScriptableObject.CreateInstance<TimelineEzEventsConfigurationAsset>();
                if (!AssetDatabase.IsValidFolder("Assets/Resources"))
                    AssetDatabase.CreateFolder("Assets", "Resources");
                if (!AssetDatabase.IsValidFolder("Assets/Resources/Bizzini"))
                    AssetDatabase.CreateFolder("Assets/Resources", "Bizzini");
                if (!AssetDatabase.IsValidFolder("Assets/Resources/Bizzini/" + StoreFolder))
                    AssetDatabase.CreateFolder("Assets/Resources/Bizzini", StoreFolder);
                AssetDatabase.CreateAsset(_config, "Assets/Resources/Bizzini/" + StoreFolder + "/" + configFileName);
            }
            EditorUtility.SetDirty(_config);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public static void Load()
        {
            _config = AssetDatabase.LoadAssetAtPath<TimelineEzEventsConfigurationAsset>("Assets/Resources/" + StoreFolder + "/" + configFileName);
            if (_config == null)
                Save();
        }

        public static bool MethodsLogs
        {
            get
            {
                return Config.LogMethodCalls;
            }
            set
            {
                Config.LogMethodCalls = value;
            }
        }

        public static bool LossMethodProtection
        {
            get
            {
                return Config.LossMethodProtection;
            }
            set
            {
                Config.LossMethodProtection = value;
            }
        }

        public static bool GroupMethodsByUnderscore
        {
            get
            {
                return Config.GroupMethodsByUnderscore;
            }
            set
            {
                Config.GroupMethodsByUnderscore = value;
            }
        }

        public static string SearchTypes
        {
            get
            {
                return Config.SearchTypes;
            }
            set
            {
                Config.SearchTypes = value;
            }
        }

        public static string SearchMethodName
        {
            get
            {
                return Config.SearchMethodName;
            }
            set
            {
                Config.SearchMethodName = value;
            }
        }

        public static bool SearchInvalidMethodsOnly
        {
            get
            {
                return Config.SearchInvalidMethodsOnly;
            }
            set
            {
                Config.SearchInvalidMethodsOnly = value;
            }
        }

        public static TimelineEzEventsConfigurationAsset Config
        {
            get
            {
                if (_config == null)
                    Load();
                return _config;
            }
        }
    }
#endif
}