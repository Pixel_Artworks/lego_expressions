using UnityEditor;
using UnityEngine;

namespace Bizzini.TimelineEzEvents.EditorStuff
{
    [CustomPropertyDrawer(typeof(TimelineEzEventsBehaviour))]
    public class TimelineEzEventsOnClipDrawer : TimelineEzEventsPropertyDrawerBase
    {
        public override void OnGUI(Rect windowRect, SerializedProperty eventsBehaviourProperty, GUIContent label)
        {
            EditorGUI.PropertyField(windowRect, eventsBehaviourProperty.FindPropertyRelative("EventsContainer"));
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            TimelineEzEventsClip clip = property.serializedObject.targetObject as TimelineEzEventsClip;
            return TimelineEzEventsGuiDrawHelper.DefaultLineHeight + calculateEventsHeight(true, clip.behaviourTemplate.EventsContainer.Events);
        }
    }
}