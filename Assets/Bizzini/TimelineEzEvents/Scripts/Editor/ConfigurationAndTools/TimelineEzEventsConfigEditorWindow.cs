namespace Bizzini.TimelineEzEvents.EditorStuff
{
    using UnityEngine;
    using UnityEditor;

    public class TimelineEzEventsConfigWindow : EditorWindow
    {
        [MenuItem("Tools/Bizzini/TimelineEzEvents/Configuration")]
        static void Init()
        {
            TimelineEzEventsConfigWindow window = (TimelineEzEventsConfigWindow)GetWindow(typeof(TimelineEzEventsConfigWindow));
            window.Show();
        }

        void OnGUI()
        {
            GUILayout.Label("Editor options", EditorStyles.boldLabel);
            EditorGUI.BeginChangeCheck();
            EditorGUIUtility.labelWidth = 250;
            TimelineEzEventsConfig.MethodsLogs = EditorGUILayout.Toggle("Log methods (Debug builds only)", TimelineEzEventsConfig.MethodsLogs);
            TimelineEzEventsConfig.GroupMethodsByUnderscore = EditorGUILayout.Toggle("Group methods by Underscore", TimelineEzEventsConfig.GroupMethodsByUnderscore);
            TimelineEzEventsConfig.LossMethodProtection = EditorGUILayout.Toggle("Loss method protection (experimental)", TimelineEzEventsConfig.LossMethodProtection);
            if (EditorGUI.EndChangeCheck())
                TimelineEzEventsConfig.Save();
        }
    }
}