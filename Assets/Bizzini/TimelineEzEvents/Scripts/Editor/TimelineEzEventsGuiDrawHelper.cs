﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Bizzini.TimelineEzEvents.EditorStuff
{
    public class TimelineEzEventsGuiDrawHelper
    {
        protected static GUIStyle _styleLabel_Title;
        protected static GUIStyle _styleLabel_EventName;
        protected static GUIStyle _styleLabel_MethodArgs;
        protected static GUIStyle _styleLabel_MethodTitle;
        protected static GUIStyle _styleLabel_InfoBoxErrorLabel;
        protected static GUIStyle _styleLabel_InfoBoxNormalLabel;
        public static Color Color_BtnEventsDelete = new Color(0.8f, 0.2f, 0.2f);
        public static Color Color_BtnEventsTools = new Color(0.8f, 0.85f, 0.8f);
        public static Color Color_BackGroundNormal = Color.white;
        public static Color Color_InfoBackGroundError = Color.red;
        public static Color Color_InfoBackGroundNormal = new Color(0.8f, 0.8f, 0.8f);
        public const float DefaultLineHeight = 18f;

        public static GUIStyle StyleLabel_Title
        {
            get
            {
                if (_styleLabel_Title == null)
                {
                    _styleLabel_Title = new GUIStyle();
                    _styleLabel_Title.fontStyle = FontStyle.Bold;
                    _styleLabel_Title.normal.textColor = Color.black;
                }
                return _styleLabel_Title;
            }
        }

        public static GUIStyle StyleLabel_EventName
        {
            get
            {
                if (_styleLabel_EventName == null)
                {
                    _styleLabel_EventName = new GUIStyle();
                    _styleLabel_EventName.fontStyle = FontStyle.Bold;
                    _styleLabel_EventName.normal.textColor = new Color(0, 0.15f, 0);
                }
                return _styleLabel_EventName;
            }
        }

        public static GUIStyle StyleLabel_MethodArgs
        {
            get
            {
                if (_styleLabel_MethodArgs == null)
                {
                    _styleLabel_MethodArgs = new GUIStyle();
                    //_styleLabel_MethodArgs.fontStyle = FontStyle.Italic;
                    _styleLabel_MethodArgs.normal.textColor = new Color(0.1f, 0.1f, 0.1f);
                }
                return _styleLabel_MethodArgs;
            }
        }

        public static GUIStyle StyleLabel_MethodTitle
        {
            get
            {
                if (_styleLabel_MethodTitle == null)
                {
                    _styleLabel_MethodTitle = new GUIStyle();
                    _styleLabel_MethodTitle.fontStyle = FontStyle.Bold;
                    _styleLabel_MethodTitle.normal.textColor = new Color(0f, 0f, 0f);
                }
                return _styleLabel_MethodTitle;
            }
        }

        public static GUIStyle StyleLabel_InfoBoxErrorLabel
        {
            get
            {
                if (_styleLabel_InfoBoxErrorLabel == null)
                {
                    _styleLabel_InfoBoxErrorLabel = new GUIStyle();
                    _styleLabel_InfoBoxErrorLabel.fontStyle = FontStyle.Normal;
                    _styleLabel_InfoBoxErrorLabel.normal.textColor = new Color(0.8f, 0, 0);
                }
                return _styleLabel_InfoBoxErrorLabel;
            }
        }

        public static GUIStyle StyleLabel_InfoBoxNormalLabel
        {
            get
            {
                if (_styleLabel_InfoBoxNormalLabel == null)
                {
                    _styleLabel_InfoBoxNormalLabel = new GUIStyle();
                    _styleLabel_InfoBoxNormalLabel.fontStyle = FontStyle.Normal;
                    _styleLabel_InfoBoxNormalLabel.normal.textColor = Color.black;
                }
                return _styleLabel_InfoBoxNormalLabel;
            }
        }

        //TO BE IMPROVED IN THE FUTURE
        public class DrawManagement
        {
            public float CurHeight;
            protected Rect _curRect;
            protected float padding;
            protected float frozenHeight;
            protected bool frozenHeightEnabled;
            public Vector2 CurRectGlobalOffset;

            public DrawManagement(float pad)
            {
                CurRect = new Rect();
                padding = pad;
            }

            public void Begin(Rect localRect)
            {
                CurHeight = 0;

                _curRect = new Rect(localRect);
                _curRect.x += padding;
                _curRect.y += padding;
                _curRect.width -= padding * 2f;
                _curRect.height -= padding * 2f;
            }

            public void BeginFreezeHeight()
            {
                frozenHeight = CurHeight;
                frozenHeightEnabled = true;
            }

            public void StopFreezeHeight()
            {
                CurHeight = frozenHeight;
                frozenHeightEnabled = false;
            }

            public void AddLine(int howMany = 1)
            {
                AddHeight(DefaultLineHeight * howMany);
            }

            public void AddHeight(float height)
            {
                if (!frozenHeightEnabled)
                    CurHeight += height;
            }

            public Rect CurRect
            {
                get
                {
                    return _curRect;
                }
                protected set
                {
                    _curRect = value;
                }
            }

            public Rect GetColumn_Horiz_Two(Rect rect, int colIndex)
            {
                switch (colIndex)
                {
                    case 0:
                        return new Rect(rect.x, rect.y, (rect.width * 0.4f), rect.height);
                    case 1:
                        return new Rect(rect.x + (rect.width * 0.4f), rect.y, (rect.width * 0.6f), rect.height);
                }
                return rect;
            }

            public Rect GetPos(Rect column, float yPos, float offsetX = 0, float? widthOverride = null)
            {
                Rect r = new Rect(column.x + offsetX, column.y + yPos, (widthOverride.HasValue ? widthOverride.Value : column.width), DefaultLineHeight * 0.9f);
                r.xMin = Mathf.Clamp(r.xMin, _curRect.xMin, _curRect.xMax);
                r.xMax = Mathf.Clamp(r.xMax, _curRect.xMin, _curRect.xMax);
                r.x += CurRectGlobalOffset.x;
                r.y += CurRectGlobalOffset.y;
                return r;
            }

            public Rect DrawEditor_Box(Rect rect, Color color)
            {
                Color oldColor = GUI.backgroundColor;
                GUI.backgroundColor = color;
                GUI.Box(rect, "");
                GUI.backgroundColor = oldColor;
                return rect;
            }

            public Rect DrawEditor_Rect(Rect rect, Color color)
            {
                Color oldColor = GUI.backgroundColor;
                GUI.backgroundColor = color;
                EditorGUI.DrawRect(rect, color);
                GUI.backgroundColor = oldColor;
                return rect;
            }

            public int DrawEditor_Enum(Type type, SerializedProperty property, string stringValue, string paramName, int defaultIndex = -1, float horizontalOffset = 0, GUIStyle labelStyle = null)
            {
                string[] names = Enum.GetNames(type);
                int[] intValues = Enum.GetValues(type).Cast<int>().ToArray();
                int enumCurIndex;
                bool isStringSerializedEnum = property != null && property.type == "string";
                enumCurIndex = isStringSerializedEnum ? stringValue.ToType<int>() : property != null ? property.enumValueIndex : 0;
                int newIndex = Math.Max(Array.IndexOf(intValues, enumCurIndex), 0);
                newIndex = DrawEditor_Dropdown(paramName, type.ToString(), newIndex, names, null, horizontalOffset, -1, labelStyle);
                if (property != null)
                {
                    if (isStringSerializedEnum)
                    {
                        if (newIndex >= 0)
                            property.stringValue = intValues[newIndex].ToString();
                        else
                            property.stringValue = defaultIndex.ToString();
                    }
                    else
                    {
                        if (newIndex >= 0)
                            property.enumValueIndex = intValues[newIndex];
                        else
                            property.enumValueIndex = defaultIndex;
                    }
                }
                return newIndex;
            }

            public float DrawEditor_FloatField(string label, string toolTip, float value, GUIStyle labelStyle = null)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                if (labelStyle != null)
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                else
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));
                value = EditorGUI.FloatField(GetPos(col1, CurHeight, 0, col1.width), value);
                AddLine();
                return value;
            }

            public double DrawEditor_DoubleField(string label, string toolTip, double value, GUIStyle labelStyle = null)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                if (labelStyle != null)
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                else
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));
                value = EditorGUI.DoubleField(GetPos(col1, CurHeight, 0, col1.width), value);
                AddLine();
                return value;
            }

            public int DrawEditor_IntField(string label, string toolTip, int value, GUIStyle labelStyle = null)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                if (labelStyle != null)
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                else
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));
                value = EditorGUI.IntField(GetPos(col1, CurHeight, 0, col1.width), value);
                AddLine();
                return value;
            }

            public string DrawEditor_TextField(string label, string toolTip, string content, GUIStyle labelStyle = null)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                if (labelStyle != null)
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                else
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));
                content = EditorGUI.TextField(GetPos(col1, CurHeight, 0, col1.width), content);
                AddLine();
                return content;
            }

            public void DrawEditor_LabelField(string label, string toolTip, int colIndex = 0, float horizontalOffset = 0, GUIStyle labelStyle = null)
            {
                Rect col = GetColumn_Horiz_Two(_curRect, colIndex);
                if (labelStyle != null)
                    EditorGUI.LabelField(GetPos(col, CurHeight, horizontalOffset), new GUIContent(label, toolTip), labelStyle);
                else
                    EditorGUI.LabelField(GetPos(col, CurHeight, horizontalOffset), new GUIContent(label, toolTip));
                AddLine();
            }

            public Vector2 DrawEditor_Vector2Field(string label, string toolTip, Vector2 v, float horizontalOffset = 0, GUIStyle labelStyle = null)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                float col1widthDivided = col1.width * (1f / 2f);
                if (labelStyle != null)
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                else
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));
                v = new Vector2(EditorGUI.FloatField(GetPos(col1, CurHeight, horizontalOffset, col1widthDivided), v.x), EditorGUI.FloatField(GetPos(col1, CurHeight, col1widthDivided, col1widthDivided), v.y));
                AddLine();
                return v;
            }

            public Vector3 DrawEditor_Vector3Field(string label, string toolTip, Vector3 v, float horizontalOffset = 0, GUIStyle labelStyle = null)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                float col1widthDivided = col1.width * (1f / 3f);
                if (labelStyle != null)
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                else
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));
                v = new Vector3(EditorGUI.FloatField(GetPos(col1, CurHeight, horizontalOffset, col1widthDivided), v.x), EditorGUI.FloatField(GetPos(col1, CurHeight, col1widthDivided, col1widthDivided), v.y), EditorGUI.FloatField(GetPos(col1, CurHeight, col1widthDivided * 2f, col1widthDivided), v.z));
                AddLine();
                return v;
            }

            public Vector4 DrawEditor_Vector4Field(string label, string toolTip, Vector4 v, float horizontalOffset = 0, GUIStyle labelStyle = null)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                float col1widthDivided = col1.width * (1f / 4f);
                if (labelStyle != null)
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                else
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));
                v = new Vector4(EditorGUI.FloatField(GetPos(col1, CurHeight, horizontalOffset, col1widthDivided), v.x), EditorGUI.FloatField(GetPos(col1, CurHeight, col1widthDivided, col1widthDivided), v.y), EditorGUI.FloatField(GetPos(col1, CurHeight, col1widthDivided * 2f, col1widthDivided), v.z), EditorGUI.FloatField(GetPos(col1, CurHeight, col1widthDivided * 3f, col1widthDivided), v.w));
                AddLine();
                return v;
            }

            public Color DrawEditor_ColorField(string label, string toolTip, Color c, float horizontalOffset = 0, GUIStyle labelStyle = null)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                if (labelStyle != null)
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                else
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));
                c = EditorGUI.ColorField(GetPos(col1, CurHeight, horizontalOffset), c);
                AddLine();
                return c;
            }

            public UnityEngine.Object DrawEditor_ObjectField(string label, string toolTip, float horizontalOffset, UnityEngine.Object obj, Type type, bool allowSceneObjects, GUIStyle labelStyle = null)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                if (labelStyle != null)
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                else
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));

                obj = EditorGUI.ObjectField(GetPos(col1, CurHeight, horizontalOffset, col1.width), new GUIContent("", toolTip), obj, type, allowSceneObjects);
                AddLine();
                return obj;
            }

            public bool DrawEditor_Toggle(string label, string toolTip, bool value, float collisionWidth = 16f, float horizontalOffset = 0, GUIStyle labelStyle = null)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                if (!string.IsNullOrEmpty(label))
                {
                    if (labelStyle != null)
                        EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                    else
                        EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));
                }
                value = EditorGUI.Toggle(GetPos(col1, CurHeight, horizontalOffset, collisionWidth), new GUIContent("", toolTip), value);
                AddLine();
                return value;
            }

            public bool DrawEditor_ToggleCol1(string label, string toolTip, bool value, float collisionWidth = 16f, float horizontalOffset = 0, float HorizAlignPercentage = 0.5f, GUIStyle labelStyle = null)
            {
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                Rect rectLabel, rectToggle;

                rectToggle = new Rect(GetPos(col1, CurHeight, (col1.width * HorizAlignPercentage) - 12f, collisionWidth));
                rectLabel = new Rect(rectToggle);
                rectLabel.width = collisionWidth;
                rectLabel.x -= collisionWidth - 16f;
                if (!string.IsNullOrEmpty(label))
                {
                    if (labelStyle != null)
                        EditorGUI.LabelField(rectLabel, new GUIContent(label, toolTip), labelStyle);
                    else
                        EditorGUI.LabelField(rectLabel, new GUIContent(label, toolTip));
                }
                value = EditorGUI.Toggle(rectToggle, new GUIContent("", toolTip), value);
                AddLine();
                return value;
            }

            public int DrawEditor_Dropdown(string label, string toolTip, int index, string[] stringValues, GUIContent[] guiContentValues, float horizontalOffset, float width = -1, GUIStyle labelStyle = null)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                if (labelStyle != null)
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                else
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));
                if (stringValues != null)
                    index = EditorGUI.Popup(GetPos(col1, CurHeight, horizontalOffset, width > -1 ? width : col1.width - horizontalOffset), index, stringValues);
                else
                    index = EditorGUI.Popup(GetPos(col1, CurHeight, horizontalOffset, width > -1 ? width : col1.width - horizontalOffset), index, guiContentValues);
                AddLine();
                return index;
            }

            public bool DrawEditor_Button(string label, string btnLabel, string toolTip, Color? bgColor = null, float width = -1, float heightOnLineHeightPerc = 0.9f, float HorizAlignPercentage = 0, float HorizPosOffset = 0, GUIStyle labelStyle = null)
            {
                Color oldColor = GUI.backgroundColor;
                if (bgColor.HasValue)
                    GUI.backgroundColor = bgColor.Value;
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);

                if (labelStyle != null)
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                else
                    EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));

                float btnWidth = width > -1 ? width : col1.width;
                Rect btnRect = GetPos(col1, CurHeight, ((col1.width * HorizAlignPercentage) - (btnWidth * HorizAlignPercentage) + (HorizPosOffset)), btnWidth);
                btnRect.height = DefaultLineHeight * heightOnLineHeightPerc;

                bool btnState = GUI.Button(btnRect, new GUIContent(btnLabel, toolTip));
                GUI.backgroundColor = oldColor;
                AddLine();
                return btnState;
            }

            public bool DrawEditor_Button(string btnLabel, string toolTip, int columnIndex, Color? bgColor = null, float width = -1, float heightOnLineHeightPerc = 0.9f, float HorizAlignPercentage = 0.5f, float HorizPosOffset = 0)
            {
                Color oldColor = GUI.backgroundColor;
                if (bgColor.HasValue)
                    GUI.backgroundColor = bgColor.Value;
                Rect col = GetColumn_Horiz_Two(_curRect, columnIndex);
                float btnWidth = width > -1 ? width : col.width;
                Rect btnRect = GetPos(col, CurHeight, ((col.width * HorizAlignPercentage) - (btnWidth * HorizAlignPercentage) + (HorizPosOffset)), btnWidth);
                btnRect.height = DefaultLineHeight * heightOnLineHeightPerc;
                bool btnState = GUI.Button(btnRect, new GUIContent(btnLabel, toolTip));
                GUI.backgroundColor = oldColor;
                AddLine();
                return btnState;
            }

            public void DrawEditor_HelpBox(string msg, MessageType msgType)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                EditorGUI.HelpBox(GetPos(col0, CurHeight), msg, msgType);
                AddLine();
            }

            public AnimationCurve DrawEditor_Curve(string label, string toolTip, AnimationCurve value, float? collisionWidth = null, float horizontalOffset = 0, GUIStyle labelStyle = null)
            {
                Rect col0 = GetColumn_Horiz_Two(_curRect, 0);
                Rect col1 = GetColumn_Horiz_Two(_curRect, 1);
                if (!string.IsNullOrEmpty(label))
                {
                    if (labelStyle != null)
                        EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip), labelStyle);
                    else
                        EditorGUI.LabelField(GetPos(col0, CurHeight), new GUIContent(label, toolTip));
                }
                value = EditorGUI.CurveField(GetPos(col1, CurHeight, horizontalOffset, collisionWidth), new GUIContent("", toolTip), value);
                AddLine();
                return value;
            }
        }
    }
}