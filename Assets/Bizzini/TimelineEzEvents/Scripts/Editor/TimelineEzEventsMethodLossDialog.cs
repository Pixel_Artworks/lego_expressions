using UnityEditor;
using UnityEngine;

namespace Bizzini.TimelineEzEvents.EditorStuff
{
    public static class TimelineEzEventsMethodLossDialog
    {
        public static bool Prompt(string lostMethod, string similarMethod)
        {
            return showDialog(lostMethod, similarMethod);
        }

        private static bool showDialog(string lostMethod, string similarMethod)
        {
            return EditorUtility.DisplayDialog("TimelineEzEvents - Method not found", "Lost\n" + lostMethod + "\n\nBest match is\n" + similarMethod, "REPLACE", "IGNORE");
        }
    }
}