﻿using Bizzini.TimelineEzEvents.EditorStuff;
using UnityEditor;
using UnityEngine;

class GizmoCopy : AssetPostprocessor
{
    private string gizmoTrack_FileName = "TimelineEzEventsTrack.png";

    private void OnPreprocessTexture()
    {
        if (assetPath.Contains("Bizzini/TimelineEzEvents/Gizmos"))
        {
            if (!AssetDatabase.IsValidFolder("Assets/Gizmos"))
                AssetDatabase.CreateFolder("Assets", "Gizmos");
            if (!System.IO.File.Exists(Application.dataPath + "/Gizmos/" + gizmoTrack_FileName))
            {
                AssetDatabase.CopyAsset("Assets/Bizzini/TimelineEzEvents/Gizmos/TimelineEzEventsTrack.png", "Assets/Gizmos/TimelineEzEventsTrack.png");
                //System.IO.File.Copy(Application.dataPath + "/Bizzini/TimelineEzEvents/Gizmos/" + gizmoTrack_FileName, Application.dataPath + "/Gizmos/" + gizmoTrack_FileName);
            }
        }
    }
}