﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.Playables;

namespace Bizzini.TimelineEzEvents
{
    public class TimelineEzEventsInvocationManagement
    {
        private TimelineEzEventsHelper.BehaviourMethodData targetBehaviourConditionMethodData;
        private TimelineEzEventsHelper.BehaviourMethodData targetBehaviourMethodData;
        private TimelineEzEventsHelper.BehaviourMethodData targetBehaviourAlternateMethodData;
        private EzEvent linkedEvent;
        private object[][] cachedInvokeArgs;
        public FrameData CurrentFrameData;
        private int frameDataArgPos = -1;

        public TimelineEzEventsInvocationManagement(EzEvent evt, TimelineEzEventsHelper.BehaviourMethodData behaviourConditionMethodData, TimelineEzEventsHelper.BehaviourMethodData behaviourMethodData, TimelineEzEventsHelper.BehaviourMethodData behaviourAlternateMethodData)
        {
            linkedEvent = evt;
            targetBehaviourConditionMethodData = behaviourConditionMethodData;
            targetBehaviourMethodData = behaviourMethodData;
            targetBehaviourAlternateMethodData = behaviourAlternateMethodData;
            cachedInvokeArgs = new object[3][];
        }

        public object Invoke()
        {
            bool conditionResult = true;

            if (targetBehaviourConditionMethodData.HasData())
            {
                object returnObj = callMethod(0, targetBehaviourConditionMethodData);
                conditionResult = (returnObj is bool) ? (bool)returnObj : returnObj != null;
                if (linkedEvent.ConditionResultInverted)
                    conditionResult = !conditionResult;
            }

            if (conditionResult)
                return callMethod(1, targetBehaviourMethodData);
            else
                return callMethod(2, targetBehaviourAlternateMethodData);
        }

        private object callMethod(int methodIndex, TimelineEzEventsHelper.BehaviourMethodData behaviourMethodData)
        {
            object returnObject = null;

            if (behaviourMethodData.HasData())
            {
                UnityEngine.Object targetObject = null;
                MethodInfo targetMethodInfo = behaviourMethodData.MethodInfos[0];
                targetObject = behaviourMethodData.TargetObject;

                if (cachedInvokeArgs[methodIndex] == null || Application.isEditor)
                {
#if UNITY_EDITOR
                    try
                    {
#endif
                    cachedInvokeArgs[methodIndex] = new object[linkedEvent.MethodConfigurations[methodIndex].ArgValues.Length];
                    ParameterInfo[] methodParams = targetMethodInfo.GetParameters();
                    int argObjCnt = 0;
                    for (int i = 0; i < methodParams.Length; i++)
                    {
                        Type paramType = methodParams[i].ParameterType;
                        if (paramType == typeof(UnityEngine.Object))
                        {
                            EzEvent.ObjectExposedReferenceManagement.ObjectExposedInfo objExposedInfo = linkedEvent.ObjectExposedReferenceManager.Get(methodIndex, argObjCnt);
                            cachedInvokeArgs[methodIndex][i] = objExposedInfo.ResolvedObj;
                            argObjCnt++;
                        }
                        else if (paramType == typeof(FrameData))
                        {
                            frameDataArgPos = argObjCnt;
                            cachedInvokeArgs[methodIndex][i] = CurrentFrameData;
                        }
                        else if (paramType == typeof(EzEvent))
                            cachedInvokeArgs[methodIndex][i] = linkedEvent;
                        else
                            cachedInvokeArgs[methodIndex][i] = getArgFromString(paramType, linkedEvent.MethodConfigurations[methodIndex].ArgValues[i]);
                    }
                    returnObject = targetMethodInfo.Invoke(targetObject, cachedInvokeArgs[methodIndex]);
                    if (Debug.isDebugBuild)
                        Debugging.TimelineEzEventsDebugger.OnMethodInvoked(Array.IndexOf(linkedEvent.LinkedEzEventsContainer.Events, linkedEvent), methodIndex == 0, linkedEvent.ConditionResultInverted, returnObject, linkedEvent.LinkedEzEventsContainer.OperateMode, targetMethodInfo, targetObject, cachedInvokeArgs[methodIndex]);
#if UNITY_EDITOR
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Exception while executing timeline event: " + targetMethodInfo.Name + " " + e);
                    }
#endif
                }
                else
                {
#if UNITY_EDITOR
                    try
                    {
#endif
                    if (linkedEvent.OnClipInvocationTime == EzEvent.OnClipInvocationTimeEnum.EachFrame)
                        if (frameDataArgPos > -1 && frameDataArgPos < cachedInvokeArgs[methodIndex].Length)
                            cachedInvokeArgs[methodIndex][frameDataArgPos] = CurrentFrameData;
                    returnObject = targetMethodInfo.Invoke(targetObject, cachedInvokeArgs[methodIndex]);
                    if (Debug.isDebugBuild)
                        Debugging.TimelineEzEventsDebugger.OnMethodInvoked(Array.IndexOf(linkedEvent.LinkedEzEventsContainer.Events, linkedEvent), methodIndex == 0, linkedEvent.ConditionResultInverted, returnObject, linkedEvent.LinkedEzEventsContainer.OperateMode, targetMethodInfo, targetObject, cachedInvokeArgs[methodIndex]);
#if UNITY_EDITOR
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Exception while executing timeline event: " + targetMethodInfo.Name + " " + e);
                    }
#endif
                }
            }
            return returnObject;
        }

        private object getArgFromString(Type paramType, string value)
        {
            object obj = null;
            if (paramType.IsEnum)
                obj = value.ToType<int>();
            else if (IsArgOfSupportedType(paramType))
            {
                if (paramType == typeof(string))
                    obj = value;
                else if (paramType == typeof(int))
                    obj = value.ToType<int>();
                else if (paramType == typeof(uint))
                    obj = value.ToType<uint>();
                else if (paramType == typeof(Int64))
                    obj = value.ToType<Int64>();
                else if (paramType == typeof(float))
                    obj = value.ToType<float>();
                else if (paramType == typeof(double))
                    obj = value.ToType<double>();
                else if (paramType == typeof(bool))
                    obj = value.ToType<bool>();
                else if (paramType == typeof(Vector2))
                    obj = value.ToType<Vector2>();
                else if (paramType == typeof(Vector3))
                    obj = value.ToType<Vector3>();
                else if (paramType == typeof(Vector4))
                    obj = value.ToType<Vector4>();
                else if (paramType == typeof(Color))
                    obj = value.ToType<Color>();
                else
                    Debug.Log("Argument parse error for value: " + value + " inside method " + targetBehaviourMethodData.MethodInfos[0].Name);
            }
            else
                Debug.Log("Argument parse error for value: " + value + " inside method " + targetBehaviourMethodData.MethodInfos[0].Name);

            return obj;
        }

        public static bool IsArgOfSupportedType(ParameterInfo[] paramInfos)
        {
            bool isSupported = true;
            for (int i = 0; i < paramInfos.Length; i++)
            {
                ParameterInfo paramInfo = paramInfos[i];
                if (!IsArgOfSupportedType(paramInfo.ParameterType))
                    return false;
            }
            return isSupported;
        }

        public static bool IsArgOfSupportedType(Type type)
        {
            return (type == typeof(EzEvent) || (type == typeof(FrameData) || type == typeof(UnityEngine.Object) || type == typeof(string) || type == typeof(int) || type == typeof(uint) || type == typeof(Int64) || type == typeof(float) || type == typeof(double) || type == typeof(bool) || type == typeof(Vector2) || type == typeof(Vector3) || type == typeof(Vector4) || type == typeof(Color) || type.IsEnum));
        }
    }
}