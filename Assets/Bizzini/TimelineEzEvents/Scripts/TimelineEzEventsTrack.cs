using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Bizzini.TimelineEzEvents
{
    [TrackColor(0.7f, 1f, 0.7f)]
    [TrackClipType(typeof(TimelineEzEventsClip))]
    [TrackBindingType(typeof(GameObject))]
    public class TimelineEzEventsTrack : TrackAsset
    {
        [NonSerialized] public GameObject GenericBindingGameObject;
        [NonSerialized] public PlayableDirector LinkedPlayableDirector;
        [NonSerialized] public int IndexOnTimeline = -1;
        [NonSerialized] public int PrevIndexOnTimeline = -1;
        [NonSerialized] public TimelineEzEventsMixerBehaviour MixerBehaviour;
        private List<string> clipGuids;

        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            clipGuids = new List<string>();
            ScriptPlayable<TimelineEzEventsMixerBehaviour> mixerScriptPlayable = ScriptPlayable<TimelineEzEventsMixerBehaviour>.Create(graph, inputCount);
            MixerBehaviour = mixerScriptPlayable.GetBehaviour();
            MixerBehaviour.LinkedToTrack = this;
            LinkedPlayableDirector = graph.GetResolver() as PlayableDirector;
            GenericBindingGameObject = LinkedPlayableDirector.GetGenericBinding(this) as GameObject;

            TimelineEzEventsHelper.AssignTracksIndex(timelineAsset);

#if UNITY_EDITOR
            if (PrevIndexOnTimeline > -1 && IndexOnTimeline != PrevIndexOnTimeline)
                ClearExposedReferences();
#endif

            int cnt = 0;
            foreach (TimelineClip clip in GetClips())
            {
                TimelineEzEventsClip ezEventClip = clip.asset as TimelineEzEventsClip;
                if (!string.IsNullOrEmpty(ezEventClip.UniqueId))
                {
                    if (clipGuids.Contains(ezEventClip.UniqueId) || ((ezEventClip.behaviourTemplate.LinkedEzClip == null && ezEventClip.LinkedEzTrack != null && ezEventClip.LinkedEzTrack != this)))
                        ezEventClip.AssignUniqueId();
                }
                else
                    ezEventClip.AssignUniqueId();
                if (!clipGuids.Contains(ezEventClip.UniqueId))
                    clipGuids.Add(ezEventClip.UniqueId);
                ezEventClip.LinkedEzTrack = this;
                ezEventClip.IndexOnTrack = cnt;
                ezEventClip.LinkedTimelineClip = clip;
                string clipName = "Event [" + IndexOnTimeline + "," + cnt + "] " + " (" + (ezEventClip.behaviourTemplate.EventsContainer != null && ezEventClip.behaviourTemplate.EventsContainer.Events != null ? ezEventClip.behaviourTemplate.EventsContainer.Events.Length : 0) + ")";
                clip.displayName = clipName;
                cnt++;
            }
            return mixerScriptPlayable;
        }

        public void OnTimeJump(double newTime)
        {
            TimelineEzEventsClip ezClip = TimelineEzEventsHelper.GetPreviousEzClipInTimeline(this, newTime);
            if (ezClip != null)
                MixerBehaviour.TimeJumpNewLastClipIndex = ezClip.IndexOnTrack;
            else
                MixerBehaviour.TimeJumpNewLastClipIndex = -1;
        }

#if UNITY_EDITOR
        public void ClearExposedReferences(TimelineClip excludeClip = null)
        {
            foreach (TimelineClip clip in GetClips())
            {
                if (excludeClip == null || excludeClip != clip)
                {
                    TimelineEzEventsClip ezEventClip = clip.asset as TimelineEzEventsClip;
                    if (ezEventClip != null)
                        ezEventClip.ClearTimelineClipExposedReferences();
                }
            }
        }

        private void OnDestroy()
        {
            ClearExposedReferences();
        }
#endif
    }
}