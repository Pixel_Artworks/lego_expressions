﻿using System;
using System.Reflection;
using UnityEngine;

namespace Bizzini.TimelineEzEvents.Debugging
{
    public static class TimelineEzEventsDebugger
    {
        private static EventHandler<MethodInvokeEventArg> methodInvokeHandler;

        public static void AddListener_OnMethodInvoke(EventHandler<MethodInvokeEventArg> callback)
        {
            methodInvokeHandler += callback;
        }

        public static void OnMethodInvoked(int eventIndex, bool isConditionMethod, bool conditionInverted, object returnObj, TimelineEzEventsContainer.OperatingOnEnum operateMode, MethodInfo methodInfo, UnityEngine.Object targetObject, object[] args)
        {
#if UNITY_EDITOR
            if (EditorStuff.TimelineEzEventsConfig.MethodsLogs)
#endif
            {
                bool conditionResult = ((returnObj is bool) ? (bool)returnObj : returnObj != null);
                if (conditionInverted)
                    conditionResult = !conditionResult;
                string logText = "TimelineEzEvents (" + operateMode + ")" + (isConditionMethod ? "(Condition-><b>" + (conditionResult ? "True" : "False") + "</b>)" : "") + "[<b> " + eventIndex + " </b>] invoked:<color=white><b> " + methodInfo.DeclaringType + "." + methodInfo.Name + " </b></color> on: <b> " + targetObject.name + " </b> " + (args != null && args.Length > 0 ? " <i> args: </i> " + TimelineEzEventsHelper.MethodArgsToPrettyString(args) : "");
                Debug.Log(logText, targetObject);
                if (methodInvokeHandler != null)
                    methodInvokeHandler.Invoke(targetObject, new MethodInvokeEventArg(methodInfo, logText));
            }
        }

        public static void ConsoleLogLostMethod(bool asError, GameObject targetGameObject, string methodName, string additional = "")
        {
            string s = "TimelineEzEvents - Method :<b>" + methodName + "</b>  not found on <i>" + targetGameObject + "</i>. Probably it has been renamed, deleted or arguments changed. Use Search tools to find all the invalid methods or just inspect the event and fix it." + additional;
            if (asError)
                Debug.LogError(s, targetGameObject);
            else
                Debug.LogWarning(s, targetGameObject);
        }

        public static void ConsoleLogClipSkipped(string directorName, string clipName)
        {
            string s = "TimelineEzEvents - " + directorName + " -> " + clipName + " executed afterward. Probably framerate drop occured.";
            Debug.LogWarning(s);
        }

        public class MethodInvokeEventArg : EventArgs
        {
            public MethodInfo MethodInf;
            public string outputText;

            public MethodInvokeEventArg(MethodInfo methodInfo, string output)
            {
                MethodInf = methodInfo;
                outputText = output;
            }
        }
    }
}