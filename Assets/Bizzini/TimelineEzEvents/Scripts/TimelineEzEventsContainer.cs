using System;
using UnityEngine;
using UnityEngine.Playables;

namespace Bizzini.TimelineEzEvents
{
    [Serializable]
    public class TimelineEzEventsContainer
    {
        public enum OperatingOnEnum { None, TimelineClip, Manual }
        public OperatingOnEnum OperateMode;
        public EzEvent[] Events;
        public PlayableDirector LinkedPlayableDirector;
        public GameObject TargetGameObject;
        public bool ClipTimeSnap;
        private PlayState directorPreviousPlayState;
        private bool directorDidFirstPlay;
        private object[] results;

        public void Init()
        {
            results = new object[Events.Length];
            for (int i = 0; i < Events.Length; i++)
                Events[i].Init(this, LinkedPlayableDirector, i, null, null);
        }

        public object[] InvokeEvents()
        {
            if (results != null)
            {
                for (int i = 0; i < Events.Length; i++)
                {
                    if (Events[i].IsEnabled)
                        results[i] = Events[i].Invoke(new FrameData());
                    else
                        results[i] = null;
                }
            }
            else
            {
                for (int i = 0; i < Events.Length; i++)
                {
                    if (Events[i].IsEnabled)
                        Events[i].Invoke(new FrameData());
                }
            }
            return results;
        }
    }
}